// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameGameModeBase.generated.h"

class AArena;
class AFood;
/**
 * 
 */

UCLASS()
class SNAKEGAME_API ASnakeGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:
	ASnakeGameGameModeBase();
	void BeginPlay();
	void SpawnArena();
	void SpawnFood();

	UPROPERTY(BlueprintReadWrite)
	AArena* SuperMesh;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AArena>SuperMeshActorClass;
	UPROPERTY(BlueprintReadWrite)
	AFood* MeshComponent;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood>MeshComponentFoodActorClass;
	float ArenaWeight = 98.f;
	float ArenaHight = 40.f;
	
};
