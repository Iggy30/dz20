// Copyright Epic Games, Inc. All Rights Reserved.


#include "SnakeGameGameModeBase.h"
#include "Arena.h"
#include "Food.h"




ASnakeGameGameModeBase::ASnakeGameGameModeBase()
{
	
	
}

void ASnakeGameGameModeBase::SpawnArena()
{
	FTransform NewTransform= FTransform(FRotator(),FVector(0,0,-260),FVector(ArenaHight,ArenaWeight,1));
	SuperMesh = GetWorld()->SpawnActor<AArena>(AArena::StaticClass(), NewTransform,FActorSpawnParameters());
}



void ASnakeGameGameModeBase::SpawnFood()
{
	FTransform NewTransform = FTransform(FRotator(), FVector());
	FVector SpawnLocation;
	SuperMesh->GetRandomFoodSpawnLoc(SpawnLocation);
	GetWorld()->SpawnActor<AFood>(AFood::StaticClass(), SpawnLocation,FRotator(), FActorSpawnParameters());
	
}

void ASnakeGameGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	SpawnArena();
	SpawnFood();
}
