// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Math.h"
#include "SnakeGameGameModeBase.h"
#include "Components/MeshComponent.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("/Game/SnakeGameModeBase/Content/FoodBP"));
	ConstructorHelpers::FObjectFinder<UStaticMesh>MeshAsset(TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'"));
	UStaticMesh* Asset = MeshAsset.Object;
	Material = ConstructorHelpers::FObjectFinderOptional<UMaterial>(TEXT("/Material'/Engine/MapTemplates/Materials/BasicAsset03.BasicAsset03'")).Get();
	MeshComponent->SetStaticMesh(Asset);
	MeshComponent->SetMaterial(0, Material);
	
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			ASnakeGameGameModeBase* NewSpawn = Cast<ASnakeGameGameModeBase>(GetWorld()->GetAuthGameMode());
			if (NewSpawn)
			{
				NewSpawn->SpawnFood();
			}
			Destroy(true);

		}
	}
}

