// Fill out your copyright notice in the Description page of Project Settings.


#include "Arena.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMeshActor.h"


// Sets default values
AArena::AArena()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SuperMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("/Game/SnakeGameModeBase/Content/ArenaBP"));
	ConstructorHelpers::FObjectFinder<UStaticMesh>MeshAsset(TEXT("StaticMesh'/Engine/BasicShapes/Cube1.Cube1'"));
	UStaticMesh* Asset = MeshAsset.Object;

	SuperMesh->SetStaticMesh(Asset);
	
}

// Called when the game starts or when spawned
void AArena::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AArena::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FVector ArenaHight;
	FVector ArenaWeight;
	GetActorBounds(false, ArenaHight, ArenaWeight);
}


void AArena::GetRandomFoodSpawnLoc(FVector& OutLocation)
{
	FVector ArenaHight;
	FVector ArenaWeight;
	GetActorBounds(false, ArenaHight, ArenaWeight);
	float SpawnZ = 0;
	float SpawnX = FMath::RandRange(ArenaHight.X - ArenaWeight.X, ArenaHight.X + ArenaWeight.X);
	float SpawnY = FMath::RandRange(ArenaHight.Y - ArenaWeight.Y, ArenaHight.Y + ArenaWeight.Y);
	OutLocation = FVector(SpawnX, SpawnY, SpawnZ);
}

